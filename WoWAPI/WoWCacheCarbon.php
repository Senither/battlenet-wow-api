<?php

class WoWCacheCarbon
{
    /**
     * Placeholder for the current time for each Carbon object.
     * @var Integer
     */
    private $time;

    /**
     * A list of all seven days in plain english, this is used to help match days with user imput.
     * @var Array
     */
    private $days = array(
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Satureday',
        'Sunday'
    );

    /**
     * A list of all twelve months in plain english, this is used to hlep match days with use imput.
     * @var Array
     */
    private $months = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );

    /**
     * Sets our timezone for us so we can make sure all of our times will match,
     * regardless of the server we're on and it's time.
     * @param Object $time The time() time or a strtotime string.
     */
    public function __construct($time = null)
    {
        ini_set('date.timezone', 'Europe/Berlin');

        if ($time != null) {
            $this->time = $time;
        }
    }

    /**
     * Generates a new Carbon instances with the current time
     * @return Carbon The new Carbon instance
     */
    public static function now()
    {
        return new self(gmdate('U'));
    }

    /**
     * Generates a new Carbon instances 24 hours into the future
     * @return Carbon The new Carbon instance
     */
    public static function tomorrow()
    {
        return new self(gmdate('U') + (60 * 60 * 24));
    }

    /**
     * Generates a new Carbon instances 24 hours into the past
     * @return Carbon The new Carbon instance
     */
    public static function yesterday()
    {
        return new self(gmdate('U') - (60 * 60 * 24));
    }

    /**
     * This will generate a Carbon instance with the given time value.
     * @param  Integer $time The time() integer
     * @return Carbon       The new Carbon instance
     */
    public static function createFrom($time)
    {
        if (is_int($time)) {
            return new self($time);
        }
        return new self(strtotime($time));
    }

    // Modifiers - Seconds

    public function addSecond()
    {
        $this->time += 1;
        return $this;
    }

    public function subSecond()
    {
        $this->time -= 1;
        return $this;
    }

    public function addSeconds($seconds = 1)
    {
        $this->time += $seconds;
        return $this;
    }

    public function subSeconds($seconds = 1)
    {
        $this->time -= $seconds;
        return $this;
    }

    // Modifiers - Minutes

    public function addMinute()
    {
        $this->time += 60;
        return $this;
    }

    public function subMinute()
    {
        $this->time -= 60;
        return $this;
    }

    public function addMinutes($minutes = 1)
    {
        $this->time += 60 * $minutes;
        return $this;
    }

    public function subMinutes($minutes = 1)
    {
        $this->time -= 60 * $minutes;
        return $this;
    }

    // Modifiers - Hours

    public function addHour()
    {
        $this->time += 60 * 60;
        return $this;
    }

    public function subHour()
    {
        $this->time -= 60 * 60;
        return $this;
    }

    public function addHours($hours = 1)
    {
        $this->time += 60 * 60 * $hours;
        return $this;
    }

    public function subHours($hours = 1)
    {
        $this->time -= 60 * 60 * $hours;
        return $this;
    }

    // Modifiers - Days

    public function addDay()
    {
        $this->time = $this->modify('+1 day');
        return $this;
    }

    public function subDay()
    {
        $this->time = $this->modify('-1 day');
        return $this;
    }

    public function addDays($days = 1)
    {
        $this->time = $this->modify('+' . $days . ' day');
        return $this;
    }

    public function subDays($days = 1)
    {
        $this->time = $this->modify('-' . $days . ' day');
        return $this;
    }

    // Modifiers - Week

    public function addWeek()
    {
        $this->time = $this->modify('next week');
        return $this;
    }

    public function subWeek()
    {
        $this->time = $this->modify('last week');
        return $this;
    }

    public function addWeeks($weeks = 1)
    {
        $this->time = $this->modify('+' . $weeks . ' weeks');
        return $this;
    }

    public function subWeeks($weeks = 1)
    {
        $this->time = $this->modify('-' . $weeks . ' weeks');
        return $this;
    }

    // Modifiers - Month

    public function addMonth()
    {
        $this->time = $this->modify('+1 month');
        return $this;
    }

    public function subMonth()
    {
        $this->time = $this->modify('-1 month');
        return $this;
    }

    public function addMonths($month = 1)
    {
        $this->time = $this->modify('+' . $month . ' months');
        return $this;
    }

    public function subMonths($Month = 1)
    {
        $this->time = $this->modify('-' . $month . ' months');
        return $this;
    }

    // Modifiers - Year

    public function addYear()
    {
        $this->time = $this->modify('next year');
        return $this;
    }

    public function subYear()
    {
        $this->time = $this->modify('last year');
        return $this;
    }

    public function addYears($year = 1)
    {
        $this->time = $this->modify('+' . $year . ' years');
        return $this;
    }

    public function subYears($year = 1)
    {
        $this->time = $this->modify('-' . $year . ' years');
        return $this;
    }

    // Boolean checks

    public function isWeekday()
    {
        switch (date('l', $this->time)) {
            case 'Saturday':
            case 'Sunday':
                return false;

            default:
                return true;
        }
    }

    public function isWeekend()
    {
        return !$this->isWeekday();
    }

    public function isYesterday()
    {
        return date('d', gmdate('U') - (60 * 60 * 24)) === date('d', $this->time);
    }

    public function isToday()
    {
        return date('d/n:Y', gmdate('U')) === date('d/n:Y', $this->time);
    }

    public function isTomorrow()
    {
        return date('d', gmdate('U') + (60 * 60 * 24)) === date('d', $this->time);
    }

    public function isFuture()
    {
        return gmdate('U') < $this->time;
    }

    public function isPast()
    {
        return gmdate('U') > $this->time;
    }

    public function isLeapYear()
    {
        $year = date('Y', $this->time);
        return ((($year % 4) == 0) && ((($year % 100) != 0) || (($year %400) == 0)));
    }

    public function isSameDay($time)
    {
        if ($time instanceof Carbon) {
            $time = $time->getTimestamp();
        }
        return date('l', gmdate('U')) == date('l', $this->time);
    }

    public function isDay($day)
    {
        $day = ucfirst(mb_strtolower($day));
        if (in_array($day, $this->days)) {
            return date('l', $this->time) == $day;
        }
        return false;
    }

    public function isMonth($month)
    {
        $month = ucfirst(mb_strtolower($month));
        if (in_array($month, $this->months)) {
            return date('F', $this->time) == $month;
        }
        return false;
    }

    // Day

    public function getDay()
    {
        return date('l', $this->time);
    }

    public function getDayNumeric()
    {
        return (int) date('j', $this->time);
    }

    // Week

    public function getWeekNumeric()
    {
        return date('W', $this->time);
    }

    // Month

    public function getMonth()
    {
        return date('F', $this->time);
    }

    public function getMonthNumeric()
    {
        return date('n', $this->time);
    }

    // Year

    public function getYear()
    {
        return date('Y', $this->time);
    }

    // Timestamp

    public function getTimestamp()
    {
        return $this->time;
    }

    public function __toString()
    {
        return date('Y-m-d H:i:s', $this->time);
    }

    // Formatter

    private function modify($modifier)
    {
        return strtotime($modifier, $this->time);
    }
}
