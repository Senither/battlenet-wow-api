<?php

class WoWCache
{
    private $path;

    public function __construct()
    {
        $this->path = __DIR__ . '/cache/';

        if (!is_dir($this->path)) {
            mkdir($this->path, 0777, true);
        }
    }

    public function put($name, $value, WoWCacheCarbon $carbon)
    {
        $parsed = explode('.', $name);
        if (!is_array($parsed) || empty($parsed)) {
            return false;
        }

        $file        = $this->path . implode('/', $parsed);
        $temp_path    = $this->path;

        for ($i = 0; $i < (count($parsed) - 1); $i++) {
            $temp_path .= $parsed[$i] . '/';
            if (!is_dir($temp_path)) {
                mkdir($temp_path, 0777);
            }
        }

        $cache = array(
            'age' => $carbon->getTimestamp(),
            'cache' => $value
        );


        file_put_contents($file, json_encode($cache));
    }

    public function get($name, $fallback = null)
    {
        $parsed = explode('.', $name);
        $parsed = implode('/', $parsed);
        $file    = $this->path . $parsed;

        if (!file_exists($file)) {
            if (is_callable($fallback)) {
                return call_user_func_array($fallback, []);
            }
            return $fallback;
        }

        $cache = file_get_contents($file);
        $cache = json_decode($cache, true);

        if (!isset($cache['age'])) {
            if (is_callable($fallback)) {
                return call_user_func_array($fallback, []);
            }
            return $fallback;
        }

        if ($cache['age'] != -1 && WoWCacheCarbon::createFrom($cache['age'])->isPast()) {
            unlink($file);
            if (is_callable($fallback)) {
                return call_user_func_array($fallback, []);
            }
            return $fallback;
        }

        return $cache['cache'];
    }

    public function pull($name, $fallback = null)
    {
        $parsed = explode('.', $name);
        $parsed = implode('/', $parsed);
        $file    = $this->path . $parsed;

        if (!file_exists($file)) {
            if (is_callable($fallback)) {
                return call_user_func_array($fallback, []);
            }
            return $fallback;
        }

        $cache = file_get_contents($file);
        $cache = json_decode($cache, true);

        if (!isset($cache['age'])) {
            if (is_callable($fallback)) {
                return call_user_func_array($fallback, []);
            }
            return $fallback;
        }

        if ($cache['age'] != -1 && WoWCacheCarbon::createFrom($cache['age'])->isPast()) {
            unlink($file);
            if (is_callable($fallback)) {
                return call_user_func_array($fallback, []);
            }
            return $fallback;
        }

        unlink($file);
        return $cache['cache'];
    }

    public function push($name, $value)
    {
    }

    public function has($name)
    {
        $parsed = explode('.', $name);
        $parsed = implode('/', $parsed);
        $file    = $this->path . $parsed;

        if (!file_exists($file)) {
            return false;
        }

        $cache = file_get_contents($file);
        $cache = json_decode($cache);

        if (!property_exists($cache, 'age')) {
            return false;
        }

        if ($cache->age != -1 && WoWCacheCarbon::createFrom($cache->age)->isPast()) {
            unlink($file);
            return false;
        }

        return true;
    }

    public function forget($name)
    {
        $parsed = explode('.', $name);
        $parsed = implode('/', $parsed);
        $file    = $this->path . $parsed;

        if (!file_exists($file)) {
            return false;
        }

        unlink($file);
        return true;
    }

    public function forever($name)
    {
        $parsed = explode('.', $name);
        $parsed = implode('/', $parsed);
        $file    = $this->path . $parsed;

        if (!file_exists($file)) {
            return false;
        }

        $cache = file_get_contents($file);
        $cache = json_decode($cache);

        if (!property_exists($cache, 'age')) {
            return false;
        }

        $cache->age  = -1;
        $cache         = json_encode($cache);

        file_put_contents($file, $cache);
        return true;
    }

    public function extend($name, $carbon)
    {
        $parsed = explode('.', $name);
        $parsed = implode('/', $parsed);
        $file    = $this->path . $parsed;

        if (!file_exists($file)) {
            return false;
        }

        $cache = file_get_contents($file);
        $cache = json_decode($cache);

        if (!property_exists($cache, 'age')) {
            return false;
        }

        $cache->age += $carbon - gmdate('U');
        $cache         = json_encode($cache);

        file_put_contents($file, $cache);
        return true;
    }
}
