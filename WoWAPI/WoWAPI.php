<?php

// Import files
require_once __DIR__ . '/WoWCache.php';
require_once __DIR__ . '/WoWCacheCarbon.php';
require_once __DIR__ . '/WoWRegion.php';
require_once __DIR__ . '/WoWObject.php';

class WoWAPI
{
    // Key set
    private $public;
    private $secret;

    // Region
    private $region;

    private $runResources = true;

    // Contains the names of all the available resources.
    private $resource = [];

    // Used as a cache for all loaded resources.
    private $loadedResources = [];

    // All available resources. (This should be moved to the World of Warcraft client class)
    private $resources = [
        'battlegroups/'         => 'battlegroups',
        'character/races'       => 'characterRaces',
        'character/classes'     => 'characterClasses',
        'character/achievements'=> 'characterAchievements',
        'guild/rewards'         => 'guildRewards',
        'guild/perks'           => 'guildPerks',
        'guild/achievements'    => 'guildAchievements',
        'item/classes'          => 'itemClasses',
        'talents'               => 'talents',
        'pet/types'             => 'petTypes'
    ];

    // Cache handler for the WoWCache class
    private $cache;
    // Character cache data to limet the out-going calls.
    private $characterCache = [];
    private $guildCache     = [];

    public function __construct($keys = [])
    {
        $this->cache  = new WoWCache();
        $this->public = (isset($keys['public'])) ? $keys['public'] : null;
        $this->secret = (isset($keys['secret'])) ? $keys['secret'] : null;
    }

    /**
    * Load a resource and return it as an array
    */
    public function loadResource($name)
    {
        if (isset($this->loadedResources[$name])) {
            return $this->loadedResources[$name];
        }

        if (!in_array($name, $this->resources)) {
            throw new RuntimeException("Invalid resource name given!");
        }

        $source = $this->cache->get('resources.' . $name, []);
        $this->loadedResources[$name] = $source;

        return $source;
    }

    /**
    * Downloads a resource off BattleNet's API.
    */
    private function getResource($name)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://{$this->region['domain']}.battle.net/api/wow/data/{$name}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        if ($header['http_code'] != 200) {
            return false;
        }
        return $output;
    }

    public function setPublicKey($key)
    {
        $this->public = $key;
    }

    public function setSecretKey($key)
    {
        $this->secret = $key;
    }

    public function setRegion($region)
    {
        if (is_array($region) && isset($region['domain']) && isset($region['local'])) {
            $this->region = $region;
        }
    }

    public function getPublicKey()
    {
        return $this->public;
    }

    public function getSecretKey()
    {
        return $this->secret;
    }

    protected function validate($checkSecret = false)
    {
        if ($checkSecret && $this->secret == null) {
            return false;
        }

        if ($this->public == null) {
            return false;
        }

        if ($this->region != null && is_array($this->region)) {
            if (!isset($this->region['domain']) || !isset($this->region['domain'])) {
                return false;
            }
        }

        // Load/Download resources
        foreach ($this->resources as $url => $name) {
            if ($this->cache->get('resources.' . $name) == null) {
                $this->cache->put('resources.' . $name, json_decode($this->getResource($url), false), WoWCacheCarbon::now()->addMonth());
            }
            $this->resource[] = $name;
        }

        return true;
    }

    public function getCharacter($character, $realm)
    {
        if (!$this->validate()) {
            return [];
        }

        $name = $this->region['local'] . '-' . $realm . '-' . $character;

        // Checks if we have saved our cache to our character array.
        if (isset($this->characterCache[$name])) {
            return $this->characterCache[$name];
        }

        // Checks if we have a valid cache of the player on our server, will incress the page load time.
        if ($this->cache->has('characters.' . $name)) {
            $this->characterCache[$name] = $this->cache->get('characters.' . $name);
            return $this->characterCache[$name];
        }

        $response = $this->sendRequest('character', [$realm, $character], ['feed', 'guild', 'professions', 'progression', 'pvp', 'stats', 'titles', 'appearance', 'items'], true);

        // Checks to see if we got a valid character response.
        if (!isset($response['status'])) {
            // Replaces ID values with readable information from our resource cache.
            $response = $this->replaceValue('class', $response, 'characterClasses');
            $response = $this->replaceValue('race', $response, 'characterRaces');
            $response['gender'] = [
                'id'   => $response['gender'],
                'name' => ($response['gender'] == 0) ? 'Male' : 'Female'
            ];
        }

        // Sets our character cache.
        $this->characterCache[$name] = $response;

        // Save cache and return
        $this->cache->put('characters.' . $name, $response, WoWCacheCarbon::now()->addHours(12));
        return $response;
    }

    /**
    * Gets our guild request call data.
    */
    public function getGuild($guild, $realm)
    {
        if (!$this->validate()) {
            return [];
        }

        // Checks if we have a valid cache of the guild on our server, will incress the page load time.
        $name = $this->region['local'] . '-' . $realm . '-' . utf8_decode(str_replace(' ', '_', $guild));

        // Checks if we have saved our cache to our guild array.
        if (isset($this->guildCache[$name])) {
            return $this->guildCache[$name];
        }

        if ($this->cache->has('guild.' . $name)) {
            $this->guildCache[$name] = $this->cache->get('guild.' . $name);
            return $this->guildCache[$name];
        }

        // Sends our guild request call.
        $response = $this->sendRequest('guild', [$realm, $guild], ['members', 'news', 'challenge', 'achievements'], true);

        // Checks to see if we got a valid guild response.
        if (!isset($response['status'])) {
            $members = $response['members'];

            // Loops through all the members of the guild and replaces the ID values
            // with readable information from our resource cache.
            foreach ($members as $index => $member) {
                $member = $member['character'];
                $member = $this->replaceValue('class', $member, 'characterClasses');
                $member = $this->replaceValue('race', $member, 'characterRaces');
                $member['gender'] = [
                    'id'   => $member['gender'],
                    'name' => ($member['gender'] == 0) ? 'Male' : 'Female'
                ];

                $response['members'][$index]['character'] = $member;
            }
        }

        // Save cache and return
        $this->cache->put('guild.' . $name, $response, WoWCacheCarbon::now()->addHours(12));
        return $response;
    }

    /**
    * Used to replace values from a multidimensional array based off a resource and key.
    *
    * @param name The index to replace
    * @param obj The array to replace the index from
    * @param res The resource name
    */
    private function replaceValue($name, $obj, $res)
    {
        $value = $obj[$name];

        // Loads the defined resource and loops throught the content.
        foreach ($this->loadResource($res) as $k => $a) {
            foreach ($a as $i => $v) {
                if ($v['id'] == $value) {
                    $obj[$name] = [
                        'id'   => $v['id'],
                        'name' => $v['name']
                    ];
                    return $obj;
                }
            }
        }

        return $obj;
    }

    /**
    * Used to send a data request call to Blizzards WoW API.
    *
    * @param func The function to use when calling the API
    * @param parms The function parameters
    * @param selector Function selectors, used to gather extra information from the request
    * @param trim Determs if the url should be trimed for the character '/' or not
    */
    private function sendRequest($func, $param, $selector = null, $trim = false)
    {
        // Generates the request call URL.
        $url = "http://{$this->region['domain']}.battle.net/api/wow/{$func}/";
        foreach ($param as $key) {
            $url .= str_replace(' ', '%20', $key) . '/';
        }

        // Removes the '/' character at the end of the url if trim is set to true.
        $url = ($trim) ? trim($url, '/') : $url;

        // Generates the selectors if there is any.
        if ($selector != null && is_array($selector)) {
            $selectors = '';
            foreach ($selector as $key) {
                $selectors .= $key . ',';
            }
            $url .= '?fields=' . trim($selectors, ',');
        }

        $url .= "&locale={$this->region['local']}&apikey={$this->public}";

        // initialize the curl object.
        $ch = curl_init();
        // Sets our curl options.
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Sends your request to the API.
        $output = curl_exec($ch);
        // Get the header responde.
        $header = curl_getinfo($ch);
        // Close your connection.
        curl_close($ch);

        // Check if our responde code is 200,
        // We're not using this at the moment.
        if ($header['http_code'] != 200) {
            return false;
        }
        return json_decode($output, true);
    }
}
