<?php

class WoWRegion
{
    // EU-English realms
    public static $EU = [
        'domain' => 'eu',
        'local'  => 'en_GB'
    ];

    // EU-Duch realms
    public static $DE = [
        'domain' => 'eu',
        'local'  => 'de_DE'
    ];

    // EU-Spanish realms
    public static $ES = [
        'domain' => 'eu',
        'local'  => 'es_ES'
    ];

    // EU-French realms
    public static $FR = [
        'domain' => 'eu',
        'local'  => 'fr_FR'
    ];

    // EU-Italian(Switzerland) realms
    public static $IT = [
        'domain' => 'eu',
        'local'  => 'it_IT'
    ];

    // EU-Polish realms
    public static $PL = [
        'domain' => 'eu',
        'local'  => 'pl_PL'
    ];

    // EU-Portuguese realms
    public static $PT = [
        'domain' => 'eu',
        'local'  => 'pt_PT'
    ];

    // EU-Russian realms
    public static $RU = [
        'domain' => 'eu',
        'local'  => 'ru_RU'
    ];

    // WoW Region instance
    private static $instance = null;

    public static function getRegionFromName($name)
    {
        if (self::$instance == null) {
            self::$instance = new ReflectionClass('WoWRegion');
        }

        $name = mb_strtoupper($name);
        if (self::$instance->hasProperty($name)) {
            return self::$instance->getStaticPropertyValue($name);
        }
        return null;
    }
}
